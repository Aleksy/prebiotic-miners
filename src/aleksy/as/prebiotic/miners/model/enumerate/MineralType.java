package aleksy.as.prebiotic.miners.model.enumerate;

import java.awt.*;

public enum MineralType {
   COAL(Color.DARK_GRAY, -10),
   GOLD(new Color(0.9f, 0.8f, 0.3f), 3);

   private Color color;
   private int points;

   MineralType(Color color, int points) {
      this.color = color;
      this.points = points;
   }

   public Color getColor() {
      return color;
   }

   public int getPoints() {
      return points;
   }
}
