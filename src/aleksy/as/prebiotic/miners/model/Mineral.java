package aleksy.as.prebiotic.miners.model;

import aleksy.as.prebiotic.miners.model.enumerate.MineralType;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Mineral extends Something {
   private static final int SIZE = 4;
   private int points;
   private MineralType type;
   private List<Miner> collectedBy;

   public Mineral(MineralType type) {
      super(type.getColor(), SIZE);
      this.points = type.getPoints();
      this.type = type;
      collectedBy = new ArrayList<>();
   }

   public int getPoints() {
      return points;
   }

   public MineralType getType() {
      return type;
   }

   @Override
   public double getSpeed() {
      return 0;
   }

   @Override
   public double getDirection() {
      return 0;
   }

   @Override
   public void setDirection(double direction) {
   }

   @Override
   public void setSpeed(double speed) {
   }

   public void addCollectedBy(Miner miner) {
      this.collectedBy.add(miner);
   }

   public boolean wasCollectedBy(Miner miner) {
      return collectedBy.contains(miner);
   }

   @Override
   public void draw(Graphics g) {
      g.setColor(this.getColor());
      g.fillOval((int) (getX() - getSize()),
         (int) (getY() - getSize()),
         (int) getSize() * 2,
         (int) getSize() * 2);
   }

   public boolean isCollectedByAtLeastOne() {
      return !collectedBy.isEmpty();
   }
}
