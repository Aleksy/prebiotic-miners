package aleksy.as.prebiotic.miners.model;

import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Protista;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;

import java.awt.*;

public abstract class ExtendedProtista extends Protista implements SomethingInterface {
   private double x;
   private double y;
   private Color color;
   private int size;
   private double speed;
   private double direction;
   private double turnSpeed;

   public ExtendedProtista(Genotype genotype, Brain brain) {
      super(genotype, brain);
      this.x = 0;
      this.y = 0;
      this.color = createColorFromGenotype();
      this.size = createSizeFromGenotype();
      this.speed = 0.0;
      this.direction = 0.0;
   }

   public ExtendedProtista(Genotype genotype) {
      super(genotype);
      this.x = 0;
      this.y = 0;
      this.color = createColorFromGenotype();
      this.size = createSizeFromGenotype();
      this.speed = 0.0;
      this.direction = 0.0;
      this.turnSpeed = 0.0;
   }

   @Override
   public double getX() {
      return x;
   }

   @Override
   public double getY() {
      return y;
   }

   @Override
   public void setX(double x) {
      this.x = x;
   }

   @Override
   public void setY(double y) {
      this.y = y;
   }

   @Override
   public Color getColor() {
      return color;
   }

   @Override
   public double getSize() {
      return size;
   }

   @Override
   public double getSpeed() {
      return speed;
   }

   @Override
   public double getDirection() {
      return direction;
   }

   @Override
   public void setSpeed(double speed) {
      this.speed = speed;
   }

   public void setTurnSpeed(double turnSpeed) {
      this.turnSpeed = turnSpeed;
   }

   public double getTurnSpeed() {
      return turnSpeed;
   }

   @Override
   public void setDirection(double direction) {
      this.direction = direction;
   }

   @Override
   public void move() {
      x += Math.sin(direction) * speed;
      y += Math.cos(direction) * speed;
   }

   private Color createColorFromGenotype() {
      float r = this.genotype.findGeneValue("features:color_r").floatValue();
      float g = this.genotype.findGeneValue("features:color_g").floatValue();
      float b = this.genotype.findGeneValue("features:color_b").floatValue();
      return new Color(r, g, b);
   }

   protected int createSizeFromGenotype() {
      return this.genotype.findGeneValue("features:size").intValue();
   }
}
