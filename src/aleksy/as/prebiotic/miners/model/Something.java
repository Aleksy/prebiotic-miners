package aleksy.as.prebiotic.miners.model;

import java.awt.*;

public abstract class Something implements SomethingInterface {
   private double x;
   private double y;
   private Color color;
   private double size;

   public Something(Color color, int size) {
      this.color = color;
      this.size = size;
   }

   @Override
   public double getX() {
      return x;
   }

   @Override
   public double getY() {
      return y;
   }

   @Override
   public Color getColor() {
      return color;
   }

   @Override
   public double getSize() {
      return size;
   }

   @Override
   public void setX(double x) {
      this.x = x;
   }

   @Override
   public void setY(double y) {
      this.y = y;
   }

   @Override
   public void move() {
   }
}
