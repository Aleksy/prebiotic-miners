package aleksy.as.prebiotic.miners.model;

import aleksy.as.prebiotic.miners.constants.DefaultValues;
import aleksy.as.prebiotic.miners.model.enumerate.MineralType;
import aleksy.prebioticframework.common.util.EuclideanDistance;
import aleksy.prebioticframework.common.util.VectorNormalizator;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.IncorrectNumberOfInputsInNeuronException;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Miner extends ExtendedProtista implements Geneticable, SomethingInterface {
   private static final int RANGE = 100;
   public static final int VISIBILITY_ZONES = 5;

   private int goldCollected;
   private int coalCollected;
   private double coalFever;
   private boolean selected;
   private boolean canCollect;
   private int collectingPeriod;
   private int actualPeriod;
   private boolean collecting;

   public Miner(Genotype genotype, Brain brain) {
      super(genotype, brain);
      this.collectingPeriod = 50;
      this.actualPeriod = this.collectingPeriod;
      this.canCollect = true;
   }

   public Miner(Genotype genotype) {
      super(genotype);
      this.collectingPeriod = 50;
      this.actualPeriod = this.collectingPeriod;
      this.canCollect = true;
   }

   @Override
   public Geneticable newInstance(Genotype genotype) {
      return new Miner(genotype);
   }

   public boolean isMineralInRange(Mineral mineral) {
      return EuclideanDistance.between(
         Arrays.asList(mineral.getX(), mineral.getY()),
         Arrays.asList(this.getX(), this.getY())
      ) < RANGE;
   }

   public List<Double> generateInputs(List<Mineral> minerals) {
      List<Double> output = brain.getNeuralNetworks().get(0).getActualOutput();
      Double[] input = generateEmptyInputs();
      List<Mineral> mineralsInRange = minerals.stream()
         .filter(this::isMineralInRange).collect(Collectors.toList());
      Map<Integer, Mineral> mineralsAndZones = assignMineralsToZones(mineralsInRange);
      mineralsAndZones.forEach((zone, mineral) -> {
         if (!mineral.wasCollectedBy(this)) {
            Color color = mineral.getColor();
            input[(zone - 1) * 4] = 1 - 2 * color.getRed() / 255.0;
            input[(zone - 1) * 4 + 1] = 1 - 2 * color.getGreen() / 255.0;
            input[(zone - 1) * 4 + 2] = 1 - 2 * color.getBlue() / 255.0;
            input[(zone - 1) * 4 + 3] = VectorNormalizator
               .setValueBetween(EuclideanDistance.between(Arrays.asList(mineral.getX(), mineral.getY()),
                  Arrays.asList(this.getX(), this.getY())), -1.0, 1.0, 0, RANGE);
         }
      });
      if (output != null) {
         input[4 * VISIBILITY_ZONES] = output.get(0);
         input[4 * VISIBILITY_ZONES + 1] = output.get(1);
         input[4 * VISIBILITY_ZONES + 2] = output.get(2);
         input[4 * VISIBILITY_ZONES + 3] = VectorNormalizator.setValueBetween((double) actualPeriod,
            -1.0, 1.0, 0, collectingPeriod);
         input[4 * VISIBILITY_ZONES + 4] = canCollect ? 1.0 : -1.0;
      } else {
         input[4 * VISIBILITY_ZONES] = 0.0;
         input[4 * VISIBILITY_ZONES + 1] = 0.0;
         input[4 * VISIBILITY_ZONES + 2] = 0.0;
         input[4 * VISIBILITY_ZONES + 3] = 0.0;
         input[4 * VISIBILITY_ZONES + 4] = 0.0;
      }
      return Arrays.asList(input);
   }

   private Double[] generateEmptyInputs() {
      Double[] input = new Double[4 * VISIBILITY_ZONES + 5];
      for (int i = 0; i < input.length; i++) {
         input[i] = -1.0;
      }
      return input;
   }

   public void setSelected(boolean selected) {
      this.selected = selected;
   }

   private Map<Integer, Mineral> assignMineralsToZones(List<Mineral> minerals) {
      Map<Integer, Mineral> mineralsAndZones = new HashMap<>();
      minerals.forEach(mineral -> {
         double angle = calculateAngle(mineral);
         double rangeForOneZone = (2 * Math.PI) / (double) VISIBILITY_ZONES;
         double actualZone = 0.0;
         for (int i = 0; i < VISIBILITY_ZONES + 1; i++) {
            if (angle < actualZone) {
               addMineralToMap(i, mineral, mineralsAndZones);
               break;
            }
            actualZone += rangeForOneZone;
         }
      });
      return mineralsAndZones;
   }

   private void addMineralToMap(int zone, Mineral mineral, Map<Integer, Mineral> mineralsAndZones) {
      Mineral mineralInMap = mineralsAndZones.get(zone);
      if (mineralInMap == null) {
         mineralsAndZones.put(zone, mineral);
      } else {
         double distanceToNewMineral = EuclideanDistance.between(
            Arrays.asList(mineral.getX(), mineral.getY()),
            Arrays.asList(this.getX(), this.getY()));
         double distanceToOldMineral = EuclideanDistance.between(
            Arrays.asList(mineralInMap.getX(), mineralInMap.getY()),
            Arrays.asList(this.getX(), this.getY()));
         if (distanceToNewMineral > distanceToOldMineral) {
            mineralsAndZones.put(zone, mineral);
         }
      }
   }

   private double calculateAngle(Mineral mineral) {
      double distanceFromMineralToCommonPoint = EuclideanDistance
         .between(Arrays.asList(mineral.getX(), mineral.getY()),
            Arrays.asList(mineral.getX(), this.getY()));
      double distanceFromMineralToMiner = EuclideanDistance
         .between(Arrays.asList(mineral.getX(), mineral.getY()),
            Arrays.asList(this.getX(), this.getY()));
      double angleBetweenCommonLineAndZero = Math
         .asin(distanceFromMineralToCommonPoint / distanceFromMineralToMiner);
      if (mineral.getX() < this.getX() && mineral.getY() > this.getY()) {
         angleBetweenCommonLineAndZero += Math.PI / 2.0;
      }
      if (mineral.getX() < this.getX() && mineral.getY() < this.getY()) {
         angleBetweenCommonLineAndZero += Math.PI;
      }
      if (mineral.getX() > this.getX() && mineral.getY() < this.getY()) {
         angleBetweenCommonLineAndZero += 1.5 * Math.PI;
      }
      angleBetweenCommonLineAndZero -= this.getDirection();
      if (angleBetweenCommonLineAndZero > 2 * Math.PI) {
         angleBetweenCommonLineAndZero -= 2 * Math.PI;
      }
      if (angleBetweenCommonLineAndZero < 0) {
         angleBetweenCommonLineAndZero += 2 * Math.PI;
      }
      return angleBetweenCommonLineAndZero;
   }

   public Mineral process(List<Mineral> minerals) throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
      List<Double> inputs = generateInputs(minerals);
      List<Double> outputs = brain.getNeuralNetworks().get(0).f(inputs);
      double newDirection = getDirection() + getTurnSpeed();
      if (newDirection > 2 * Math.PI) {
         newDirection -= 2 * Math.PI;
      }
      if (newDirection < 0) {
         newDirection += 2 * Math.PI;
      }
      setDirection(newDirection);
      setTurnSpeed(outputs.get(0) * 0.1);
      setSpeed(VectorNormalizator.setValueBetween(outputs.get(1), 0.0, 5.0, -1.0, 1.0));
      collecting = canCollect && outputs.get(2) > 0.0;
      move();
      if (getX() < 0)
         setX(DefaultValues.FRAME_WIDTH);
      if (getX() > DefaultValues.FRAME_WIDTH)
         setX(0);
      if (getY() < 0)
         setY(DefaultValues.FRAME_HEIGHT);
      if (getY() > DefaultValues.FRAME_HEIGHT)
         setY(0);
      if (coalCollected > 0)
         coalFever += 0.1;
      setSelected(false);
      actualPeriod--;
      canCollect = actualPeriod < 10;
      if (actualPeriod == 0) {
         actualPeriod = collectingPeriod;
      }
      return processIfCollect(minerals);
   }

   private Mineral processIfCollect(List<Mineral> minerals) {
      Mineral collected = null;
      if (collecting)
         for (Mineral mineral : minerals) {
            if (EuclideanDistance.between(Arrays.asList(mineral.getX(), mineral.getY()),
               Arrays.asList(this.getX(), this.getY())) < 7) {
               if (!mineral.wasCollectedBy(this)) {
                  mineral.addCollectedBy(this);
                  if (mineral.getType() == MineralType.GOLD) {
                     if (coalCollected == 0) {
                        if (goldCollected == 0)
                           goldCollected++;
                        goldCollected += goldCollected;
                     }
                  } else {
                     coalCollected++;
                  }
                  collected = mineral;
               }
            }
         }
      return collected;
   }

   @Override
   public void draw(Graphics g) {
      if (selected) {
         g.setColor(Color.WHITE);
         g.drawRect((int) (getX() - getSize()),
            (int) (getY() - getSize()),
            (int) getSize() * 2,
            (int) getSize() * 2);
         g.drawString(Double.toString(getScore()), (int) (getX() - getSize()),
            (int) (getY() - getSize() - 10));
      }
      g.setColor(this.getColor());
      g.fillOval((int) (getX() - getSize()),
         (int) (getY() - getSize()),
         (int) getSize() * 2,
         (int) getSize() * 2);
      drawEye((int) getX(), (int) getY(), g);
   }

   private void drawEye(int x, int y, Graphics g) {
      g.setColor(Color.GRAY);
      int dx = (int) (Math.sin(getDirection()) * (getSize() / 1.9));
      int dy = (int) (Math.cos(getDirection()) * (getSize() / 1.9));
      g.fillOval(x - 3 + dx, y - 3 + dy, 6, 6);
      g.setColor(Color.DARK_GRAY);
      g.drawOval(x - 3 + dx, y - 3 + dy, 6, 6);
   }

   public double getScore() {
      return goldCollected - coalFever;
   }

   public void reset() {
      this.goldCollected = 0;
      this.coalCollected = 0;
   }
}
