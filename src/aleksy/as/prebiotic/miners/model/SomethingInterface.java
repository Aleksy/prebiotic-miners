package aleksy.as.prebiotic.miners.model;

import java.awt.*;

public interface SomethingInterface {
   double getX();

   double getY();

   Color getColor();

   double getSize();

   void move();

   void setX(double x);

   void setY(double y);

   double getSpeed();

   double getDirection();

   void setDirection(double direction);

   void setSpeed(double speed);

   void draw(Graphics g);
}
