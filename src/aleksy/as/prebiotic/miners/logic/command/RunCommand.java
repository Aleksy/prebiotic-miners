package aleksy.as.prebiotic.miners.logic.command;

import aleksy.as.commandaplicationengine.annotation.Command;
import aleksy.as.commandaplicationengine.logic.Logger;
import aleksy.as.commandaplicationengine.model.AbstractCommand;
import aleksy.as.prebiotic.miners.constants.DefaultValues;
import aleksy.as.prebiotic.miners.di.Inject;
import aleksy.as.prebiotic.miners.gui.ContentPane;
import aleksy.as.prebiotic.miners.gui.frame.PrebioticMineralsFrame;
import aleksy.as.prebiotic.miners.logic.MinerProcess;
import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.as.prebiotic.miners.model.Mineral;
import aleksy.as.prebiotic.util.CreationUtil;
import aleksy.prebioticframework.common.model.Range;
import aleksy.prebioticframework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.model.network.NeuralNetwork;
import aleksy.prebioticframework.vision.common.constants.VisionConstants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Run command class
 */
@Command(name = RunCommand.NAME, description = "runs the simulation")
public class RunCommand extends AbstractCommand {
   static final String NAME = "run";
   private static final String SUBCOMMAND_TRAIN = "train";
   private static final String SUBCOMMAND_TEST = "test";
   private static final String ARGUMENT_FILE = "f";
   private static final String FLAG_NEW = "new";
   private static final String ARGUMENT_NUMBER_OF_MINERS = "m";
   private static final String ARGUMENT_NUMBER_OF_MINERALS = "ml";
   private static final String ARGUMENT_TRAINING_PERIOD = "tp";
   private ContentPane contentPane;

   @Override
   protected String getInfo() {
      return "Runs the simulation. There are two modes of simulation:\n\n" +
         "TRAINING MODE\n" +
         "In this mode minerals are not collecting by Miners. Each Miner can\n" +
         "take each mineral one time. After that mineral will be untouchable\n" +
         "by Miner, but other Miners will can take it.\n\n" +
         "TESTING MODE\n" +
         "In this mode minerals can be collected. That means that after collision\n" +
         "with Miner mineral will not exist any more.";
   }

   @Override
   protected void run() {
      try {
//         Inject.PREB.enableEventLogging();
         List<Miner> miners = readMinersBrains();
         List<Mineral> minerals = createMinerals();
         initContentPane(minerals, miners);
         new PrebioticMineralsFrame(contentPane);
         Boolean isTestingMode = null;
         if (getSubcommand().equals(SUBCOMMAND_TEST)) {
            isTestingMode = true;
         }
         if (getSubcommand().equals(SUBCOMMAND_TRAIN)) {
            isTestingMode = false;
         }
         long trainingPeriod = DefaultValues.DEFAULT_TRAINING_PERIOD;
         if (getArguments().get(ARGUMENT_TRAINING_PERIOD) != null) {
            trainingPeriod = Long.parseLong(getArguments().get(ARGUMENT_TRAINING_PERIOD));
         }
         Thread process = new MinerProcess(miners, minerals, contentPane, isTestingMode, trainingPeriod,
            getArguments().get(ARGUMENT_NUMBER_OF_MINERALS) != null ?
               Integer.parseInt(getArguments().get(ARGUMENT_NUMBER_OF_MINERALS)) :
               DefaultValues.DEFAULT_NUMBER_OF_MINERALS);
         process.start();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   private void initContentPane(List<Mineral> minerals, List<Miner> miners) {
      contentPane = new ContentPane();
      contentPane.setSize(DefaultValues.FRAME_WIDTH, DefaultValues.FRAME_HEIGHT);
      contentPane.setBackground(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
      contentPane.setMinerals(minerals);
      contentPane.setMiners(miners);
   }


   private List<Mineral> createMinerals() {
      int numberOfMinerals = DefaultValues.DEFAULT_NUMBER_OF_MINERALS;
      if (getArguments().get(ARGUMENT_NUMBER_OF_MINERALS) != null) {
         numberOfMinerals = Integer.parseInt(getArguments().get(ARGUMENT_NUMBER_OF_MINERALS));
      }
      return CreationUtil.createMinerals(numberOfMinerals);
   }

   private List<Miner> readMinersBrains() throws Exception {
      if (getFlags().contains(FLAG_NEW)) {
         return createNewMiners();
      }
      return createMinersFromFiles();
   }

   private List<Miner> createMinersFromFiles() throws Exception {
      List<Miner> miners = new ArrayList<>();
      String filepath = DefaultValues.DEFAULT_DIRECTORY_WITH_PNN_FILES;
      if (getArguments().get(ARGUMENT_FILE) != null) {
         filepath = getArguments().get(ARGUMENT_FILE);
      }
      File dir = new File(filepath);
      File[] files = dir.listFiles();
      if (files != null) {
         Random random = new Random();
         for (int i = 0; i < files.length; i++) {
            NeuralNetwork neuralNetwork = (NeuralNetwork) Inject.PREB.prebioticSystemApi().read(files[i].getAbsolutePath());
            Brain brain = new Brain();
            brain.setNeuralNetworks(new ArrayList<>());
            brain.getNeuralNetworks().add(neuralNetwork);
            Miner miner = new Miner(createRandomGenotype(), brain);
            miner.setX(random.nextDouble() * DefaultValues.FRAME_WIDTH);
            miner.setY(random.nextDouble() * DefaultValues.FRAME_HEIGHT);
            miners.add(miner);
         }
      }
      return miners;
   }

   private List<Miner> createNewMiners() throws EmptyFunctionDefinitionException, InvalidStructureException {
      List<Miner> miners = new ArrayList<>();
      int numberOfMiners = DefaultValues.DEFAULT_NUMBER_OF_MINERS;
      if (getArguments().get(ARGUMENT_NUMBER_OF_MINERS) != null) {
         numberOfMiners = Integer.parseInt(getArguments().get(ARGUMENT_NUMBER_OF_MINERS));
      }
      Random random = new Random();
      for (int i = 0; i < numberOfMiners; i++) {
         Miner miner = new Miner(createRandomGenotype(), CreationUtil.createBrain());
         miner.setX(random.nextDouble() * DefaultValues.FRAME_WIDTH);
         miner.setY(random.nextDouble() * DefaultValues.FRAME_HEIGHT);
         miners.add(miner);
      }
      return miners;
   }

   private Genotype createRandomGenotype() {
      Random random = new Random();
      return new GenotypeBuilder().addChromosome("features")
         .addGene(random.nextDouble(), new Range(0.0, 1.0), "color_r")
         .addGene(random.nextDouble(), new Range(0.0, 1.0), "color_g")
         .addGene(random.nextDouble(), new Range(0.0, 1.0), "color_b")
         .addGene(8 + 3 * random.nextDouble(), new Range(8.0, 11.0), "size")
         .end().create();
   }

   @Override
   public void init() {
      addAvailableSubcommand(SUBCOMMAND_TRAIN, "runs in training mode");
      addAvailableSubcommand(SUBCOMMAND_TEST, "runs in test mode");
      addAvailableArgument(ARGUMENT_FILE, "name of directory with saved neural networks.\n" +
         "    Each network is a brain of the Miner. If this argument is missing,\n" +
         "    default directory will be used.");
      addAvailableArgument(ARGUMENT_NUMBER_OF_MINERS, "number of miners in simulation " +
         "(default: " + DefaultValues.DEFAULT_NUMBER_OF_MINERS + ")");
      addAvailableArgument(ARGUMENT_NUMBER_OF_MINERALS, "number of minerals in simulation " +
         "(default: " + DefaultValues.DEFAULT_NUMBER_OF_MINERALS + ")");
      addAvailableArgument(ARGUMENT_TRAINING_PERIOD, "training period in training mode " +
         "(default: " + DefaultValues.DEFAULT_TRAINING_PERIOD + ")");
      addAvailableFlag(FLAG_NEW, "starts the simulation with new miners.");
      addExampleOfUsage().argument(ARGUMENT_FILE, "C:/dir")
         .description("uses 'dir' as a directory with neural network files (*.pnn).");
   }

   @Override
   protected boolean handleErrors() {
      if (getSubcommand() == null) {
         Logger.log("Subcommand cannot be empty.");
      }
      return super.handleErrors();
   }
}
