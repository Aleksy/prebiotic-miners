package aleksy.as.prebiotic.miners.logic;

import aleksy.as.prebiotic.miners.constants.DefaultValues;
import aleksy.as.prebiotic.miners.di.Inject;
import aleksy.as.prebiotic.miners.genetic.MinersEnvironment;
import aleksy.as.prebiotic.miners.gui.ContentPane;
import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.as.prebiotic.miners.model.Mineral;
import aleksy.as.prebiotic.miners.model.enumerate.MineralType;
import aleksy.as.prebiotic.util.CreationUtil;
import aleksy.prebioticframework.common.enumerate.FileFormat;
import aleksy.prebioticframework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.prebioticframework.evolution.genetic.common.exception.GeneticAlgorithmException;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.InvalidStructureException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MinerProcess extends Thread {
   private List<Miner> miners;
   private List<Mineral> minerals;
   private ContentPane contentPane;
   private boolean isTestingMode;
   private long trainPeriod;
   private int numberOfMinerals;

   public MinerProcess(List<Miner> miners,
                       List<Mineral> minerals,
                       ContentPane contentPane,
                       Boolean isTestingMode,
                       long trainPeriod,
                       int numberOfMinerals) {
      this.minerals = minerals;
      this.miners = miners;
      this.contentPane = contentPane;
      this.isTestingMode = isTestingMode;
      this.trainPeriod = trainPeriod;
      this.numberOfMinerals = numberOfMinerals;
   }

   @Override
   public void run() {
      long actualPeriod = trainPeriod;
      long saveToFilePeriod = 1;
      while (true) {
         try {
            contentPane.setNextIteration((int) actualPeriod);
            if (actualPeriod == 0 && !isTestingMode) {
               actualPeriod = trainPeriod;
               boolean saveToFile = false;
               if (saveToFilePeriod-- == 0) {
                  saveToFilePeriod = 1;
                  saveToFile = true;
               }
               train(saveToFile);
            }
//            Thread.sleep(2);
            Miner best = miners.get(0);
            for (Miner miner : miners) {
               Mineral collected = miner.process(minerals);
               if (collected != null) {
                  if (collected.getType() == MineralType.COAL) {
                     contentPane.incrementCoal();
                  }
                  if (collected.getType() == MineralType.GOLD) {
                     contentPane.incrementGold();
                  }
               }
               if (miner.getScore() > best.getScore())
                  best = miner;
               if (isTestingMode) deleteCollectedMinerals(minerals);
            }
            best.setSelected(true);
            contentPane.setBest(best);
            contentPane.repaint();
            Inject.PREB.visionApi().repaintAll();
            actualPeriod--;
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
   }

   private void train(boolean saveToFile) throws EmptyFunctionDefinitionException, InvalidStructureException, GeneticAlgorithmException {
      minerals = CreationUtil.createMinerals(numberOfMinerals);
      contentPane.setMinerals(minerals);
      contentPane.incrementIteration();
      contentPane.resetGoldAndCoal();
      List<Geneticable> geneticables = new ArrayList<>(miners);
      List<Geneticable> newMiners = Inject.PREB.evolutionApi().geneticAlgorithm(geneticables, new MinersEnvironment(),
         Inject.PREB.evolutionApi().getGeneticAlgorithmConfigurationsBuilder().defaultConfigurations(1)
            .setSelectionMode(GeneticSelectionMode.ROULETTE)
            .setMutationChance(0.025).create(),
         CreationUtil.createBrain());
      miners.clear();
      Random random = new Random();
      newMiners.forEach(geneticable -> {
         Miner miner = (Miner) geneticable;
         miner.reset();
         miner.setX(random.nextDouble() * DefaultValues.FRAME_WIDTH);
         miner.setY(random.nextDouble() * DefaultValues.FRAME_HEIGHT);
         miners.add(miner);
      });
      if (saveToFile) {
         int i = 0;
         for (Miner miner : miners) {
            try {
               Inject.PREB.prebioticSystemApi().save(miner.getBrain()
                  .getNeuralNetworks().get(0), "defpnn/" + i++, FileFormat.PNN);
            } catch (IOException e) {
               e.printStackTrace();
            }
         }
      }
   }

   private void deleteCollectedMinerals(List<Mineral> minerals) {
      List<Integer> indexesToRemove = new ArrayList<>();
      minerals.forEach(mineral -> {
         if (mineral.isCollectedByAtLeastOne()) {
            indexesToRemove.add(minerals.indexOf(mineral));
         }
      });
      indexesToRemove.forEach(index -> {
         if (index < minerals.size())
            minerals.remove(index.intValue());
      });
   }
}
