package aleksy.as.prebiotic.miners.gui;

import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.as.prebiotic.miners.model.Mineral;
import aleksy.prebioticframework.vision.common.constants.VisionConstants;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ContentPane extends JPanel {
   private List<Miner> miners;
   private List<Mineral> minerals;
   private int iteration;
   private int gold;
   private int coal;
   private int nextIteration;
   private Miner best;

   public void setMinerals(List<Mineral> minerals) {
      this.minerals = minerals;
   }

   public void setMiners(List<Miner> miners) {
      this.miners = miners;
   }

   public List<Miner> getMiners() {
      return miners;
   }

   public void setBest(Miner best) {
      this.best = best;
   }

   public void setIterationAsZero() {
      this.iteration = 0;
   }

   public void resetGoldAndCoal() {
      this.coal = 0;
      this.gold = 0;
   }

   public void incrementIteration() {
      this.iteration++;
   }

   public void incrementGold() {
      this.gold++;
   }

   public void incrementCoal() {
      this.coal++;
   }

   public void setNextIteration(int nextIteration) {
      this.nextIteration = nextIteration;
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      this.minerals.forEach(mineral -> mineral.draw(g));
      this.miners.forEach(miner -> miner.draw(g));
      g.setColor(VisionConstants.PREBIOTIC_BACKGROUND_COLOR);
      g.fillRect(0, 0, 250, 26);
      g.setColor(Color.WHITE);
      g.drawString("ITERATION " + iteration + "   COAL: " + coal + "   GOLD: " + gold +
         "   TO NEXT ITERATION: " + nextIteration + "   BEST: " + best.getScore(), 13, 13);
   }
}
