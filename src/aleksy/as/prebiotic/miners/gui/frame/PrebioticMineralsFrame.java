package aleksy.as.prebiotic.miners.gui.frame;

import aleksy.as.prebiotic.miners.constants.Application;
import aleksy.as.prebiotic.miners.constants.DefaultValues;
import aleksy.as.prebiotic.miners.di.Inject;
import aleksy.as.prebiotic.miners.gui.ContentPane;
import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.prebioticframework.common.model.Range;
import aleksy.prebioticframework.vision.common.enumerate.DisplayMode;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;

public class PrebioticMineralsFrame extends JFrame implements MouseListener, MouseMotionListener {
   private ContentPane contentPane;

   public PrebioticMineralsFrame(ContentPane contentPane) {
      super(Application.NAME + " " + Application.VERSION);
      this.contentPane = contentPane;
      init();
   }

   private void init() {
      setContentPane(contentPane);
      contentPane.addMouseListener(this);
      contentPane.addMouseMotionListener(this);
      contentPane.setIterationAsZero();
      setLayout(null);
      setSize(DefaultValues.FRAME_WIDTH, DefaultValues.FRAME_HEIGHT);
      setLocation(50, 50);
      setVisible(true);
      setFocusable(true);
      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
   }

   @Override
   public void mouseClicked(MouseEvent e) {
      ContentPane cp = (ContentPane) contentPane;
      List<Miner> miners = cp.getMiners();
      miners.forEach(miner -> miner.setSelected(false));
      miners.forEach(miner -> {
         if (Math.abs(miner.getX() - e.getX()) < 15 && Math.abs(miner.getY() - e.getY()) < 15) {
            float[] hsb = new float[3];
            hsb = Color.RGBtoHSB(miner.getColor().getRed(), miner.getColor().getGreen(), miner.getColor().getBlue(), hsb);
            Inject.PREB.visionApi().display(miner.getBrain().getNeuralNetworks().get(0), (double) hsb[0],
               DisplayMode.STRUCTURE, new Range(-1.0, 1.0));
            miner.setSelected(true);
         }
      });
   }

   @Override
   public void mousePressed(MouseEvent e) {

   }

   @Override
   public void mouseReleased(MouseEvent e) {

   }

   @Override
   public void mouseEntered(MouseEvent e) {
   }

   @Override
   public void mouseExited(MouseEvent e) {

   }

   @Override
   public void mouseDragged(MouseEvent e) {

   }

   @Override
   public void mouseMoved(MouseEvent e) {
//      ContentPane cp = (ContentPane) contentPane;
//      List<Miner> miners = cp.getMiners();
//      miners.forEach(miner -> {
//         if (Math.abs(miner.getX() - e.getX()) < 50 && Math.abs(miner.getY() - e.getY()) < 50) {
//            miner.setSelected(true);
//         } else {
//            miner.setSelected(false);
//         }
//      });
   }
}
