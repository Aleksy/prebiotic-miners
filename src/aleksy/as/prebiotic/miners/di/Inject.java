package aleksy.as.prebiotic.miners.di;

import aleksy.prebioticframework.common.api.PrebioticApi;
import aleksy.prebioticframework.common.impl.Prebiotic;

public final class Inject {
   public static final PrebioticApi PREB = Prebiotic.newInstance();
}
