package aleksy.as.prebiotic.miners.constants;

public final class Application {
   public static final String NAME = "Prebiotic Miners";
   public static final String VERSION = "0.0.1";
   public static final String AUTHOR = "Aleksy Bernat";
   public static final String COMMAND_NAME = "pmin";
   public static final String PACKAGE = "aleksy.as.prebiotic.miners.logic.command";
}
