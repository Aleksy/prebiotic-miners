package aleksy.as.prebiotic.miners.constants;

public final class DefaultValues {
   public static final int DEFAULT_NUMBER_OF_MINERS = 20;
   public static final int DEFAULT_NUMBER_OF_MINERALS = 150;
   public static final int FRAME_WIDTH = 700;
   public static final int FRAME_HEIGHT = 550;
   public static final String DEFAULT_DIRECTORY_WITH_PNN_FILES = "defpnn/";
   public static final long DEFAULT_TRAINING_PERIOD = 1000;
}
