package aleksy.as.prebiotic.miners.genetic;

import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.prebioticframework.evolution.genetic.common.model.environment.Environment;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;

public class MinersEnvironment extends Environment {
   @Override
   public void rate(Geneticable geneticable) {
      Miner miner = (Miner) geneticable;
      miner.setFitness(miner.getScore());
      if (miner.getFitness() < 0.0)
         miner.setFitness(0.0);
   }
}
