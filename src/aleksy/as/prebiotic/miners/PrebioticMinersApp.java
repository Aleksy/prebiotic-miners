package aleksy.as.prebiotic.miners;

import aleksy.as.commandaplicationengine.exception.CaeException;
import aleksy.as.commandaplicationengine.runner.CaeRunner;
import aleksy.as.commandaplicationengine.runner.CaeRunnerBuilder;
import aleksy.as.prebiotic.miners.constants.Application;

/**
 * Prebiotic Miners App main class
 */
public class PrebioticMinersApp {

   /**
    * The main method
    *
    * @param args arguments list
    * @throws IllegalAccessException CAE illegal access
    * @throws CaeException           CAE business exception
    * @throws InstantiationException CAE instantiation
    */
   public static void main(String[] args) throws IllegalAccessException, CaeException, InstantiationException {
      new CaeRunnerBuilder().createForApplication(Application.NAME, Application.VERSION, Application.AUTHOR)
         .withCommandName(Application.COMMAND_NAME)
         .withDefaultPrefixes()
         .withPackage(Application.PACKAGE)
         .withInformation("")
         .fill(new CaeRunner() {
            @Override
            protected void globalRun(String[] strings) {
            }
         }).run(args);
   }
}
