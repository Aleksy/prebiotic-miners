package aleksy.as.prebiotic.util;

import aleksy.as.prebiotic.miners.constants.DefaultValues;
import aleksy.as.prebiotic.miners.di.Inject;
import aleksy.as.prebiotic.miners.model.Miner;
import aleksy.as.prebiotic.miners.model.Mineral;
import aleksy.as.prebiotic.miners.model.enumerate.MineralType;
import aleksy.prebioticframework.evolution.genetic.common.model.brain.Brain;
import aleksy.prebioticframework.neural.neuralnetstructure.common.enumerate.AxonFunctionRecipe;
import aleksy.prebioticframework.neural.neuralnetstructure.common.enumerate.DataNormalizationType;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.EmptyFunctionDefinitionException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.exception.InvalidStructureException;
import aleksy.prebioticframework.neural.neuralnetstructure.common.model.network.NeuralNetwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CreationUtil {
   public static List<Mineral> createMinerals(int numberOfMinerals) {
      List<Mineral> minerals = new ArrayList<>();
      Random random = new Random();
      for (int i = 0; i < numberOfMinerals; i++) {
         Mineral mineral = new Mineral(random.nextBoolean() ?
            MineralType.COAL : MineralType.GOLD);
         mineral.setX(random.nextDouble() * DefaultValues.FRAME_WIDTH);
         mineral.setY(random.nextDouble() * DefaultValues.FRAME_HEIGHT);
         minerals.add(mineral);
      }
      return minerals;
   }


   public static Brain createBrain() throws EmptyFunctionDefinitionException, InvalidStructureException {
      NeuralNetwork neuralNetwork = Inject.PREB.neuralApi().getNeuralNetworkBuilder(
         Inject.PREB.neuralApi().getStructureConfigurationsBuilder()
            .defaultFeedforwardConfigurations()
            .setAxonFunction(AxonFunctionRecipe.TANH)
            .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
            .create()
      ).addDefaultInputLayer(Miner.VISIBILITY_ZONES * 4 + 25, Miner.VISIBILITY_ZONES * 4 + 5)
         .addLayer(10)
         .addLayer(3)
         .endBuildingNetwork()
         .randomizeWeights(-1.0, 1.0)
         .create();
      Brain brain = new Brain();
      brain.setNeuralNetworks(new ArrayList<>());
      brain.getNeuralNetworks().add(neuralNetwork);
      return brain;
   }
}
